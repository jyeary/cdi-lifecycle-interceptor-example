/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example.cdi;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@Interceptable
@Interceptor
public class PostConstructInterceptor {

    private static final Logger log = Logger.getLogger(PostConstructInterceptor.class.getName());

    /**
     * This is the method that handles the {@link @PostConstruct} event. The
     * method name can be called any legal Java method name. I chose
     * <code>intercept</code> since it is an action describing what we are
     * doing.
     *
     * @param context The current invocation context.
     * @throws Exception If any {@link Exception} occurs during processing.
     */
    @PostConstruct
    public void intercept(InvocationContext context) throws Exception {

        log.setLevel(Level.FINE);
        log.info("Before call to @PostConstuct.");

        Map<String, Object> map = context.getContextData();
        for (String key : map.keySet()) {
            log.log(Level.INFO, "Context data key --> {0} data --> {1}", new Object[]{key, map.get(key)});
        }

        log.log(Level.INFO, "Examining object: {0}", context.getTarget().getClass().getName());

        // Best practice is to check if the object is an instanceof an interface.
        if (context.getTarget() instanceof Loggable) {
            Loggable loggable = (Loggable) context.getTarget();
            loggable.setLogger(Logger.getLogger(context.getTarget().getClass().getName()));
        }

        /*
         * This checks for an instanceof a specific bean. This is not the preferred approach
         * unless you are really expecting to only check for a specific class.
         */
        if (context.getTarget() instanceof IndexBean) {
            IndexBean indexBean = (IndexBean) context.getTarget();
            indexBean.setFactory(new UUIDFactory() {

                @Override
                public UUID generateUUID() {
                    return UUID.randomUUID();
                }
            });
        }

        context.proceed();
        log.info("After call to  to @PostConstuct.");
    }
}
