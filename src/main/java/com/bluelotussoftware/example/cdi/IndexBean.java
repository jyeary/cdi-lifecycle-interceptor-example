/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example.cdi;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * A page backing bean for the
 * <code>index.xhtml</code> page.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@Named
@RequestScoped
@Interceptable
public class IndexBean implements Loggable {

    private UUIDFactory factory;
    private Logger log;

    /**
     * Default no-argument constructor.
     */
    public IndexBean() {
    }

    @PostConstruct
    private void initialize() {
        // This should cause an NPE since logger is not initialized, but we will do our magic here.
        log.info("I am an injected logger");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLogger(final Logger logger) {
        this.log = logger;
    }

    /**
     * This sets the UUID factory for the bean.
     *
     * @param factory the factory to be set.
     */
    public void setFactory(final UUIDFactory factory) {
        log.log(Level.INFO, "Factory being set to {0}", factory);
        this.factory = factory;
    }

    /**
     * This returns a randomly generated {@link UUID#randomUUID()#toString()}
     *
     * @return a randomly generated UUID value, or {@code null} if the factory
     * is not initialized.
     */
    public String getUUID() {
        if (null != factory) {
            return factory.generateUUID().toString();
        } else {
            return null;
        }
    }
}
